# Obsidian-Plugin-Localization

#### Description
Obsidian-Plugin-Localization 是一款Obsidian插件本地化的辅助工具，用AutoHotkey开发而成。

库内提供各插件对应的 main.js 汉化文档、英汉字段对照表，辅助工具会识别插件id、自动下载、覆盖汉化，支持手动提取、翻译、扩展等功能。

欢迎对此项目感兴趣的朋友请联系蚕子（QQ：312815311）详细交流！

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
