﻿汉化：蚕子 QQ：312815311
=========================================
text: 'Consistent attachments and links - Settings'
text: '一致的附件和链接-设置'
setName('Move attachments with note'
setName('移动带注释的附件'
setDesc('When a note moves, move with it any attachments that are in the same folder and subfolders.'
setDesc('当便笺移动时，移动同一文件夹和子文件夹中的所有附件。'
setName('Delete unused attachments with note'
setName('删除未使用的带注释的附件'
setDesc('When the note is deleted, delete all attachments that are no longer used in other notes.'
setDesc('删除注释后，请删除其他注释中不再使用的所有附件。'
setName('Update links'
setName('更新链接'
setDesc('Update links to attachments and other notes when moving notes or attachments.'
setDesc('移动笔记或附件时，更新附件和其他笔记的链接。'
setName('Delete empty folders'
setName('删除空文件夹'
setDesc('Delete empty folders after moving notes with attachments.'
setDesc('移动带有附件的笔记后删除空文件夹。'
setName('Delete duplicate attachments while note moving'
setName('移动笔记时删除重复的附件'
setDesc('Delete attachment when moving a note if there is a file with the same name in the new folder. If disabled, file will be renamed and moved.'
setDesc('如果新文件夹中有同名文件，则在移动注释时删除附件。如果禁用，文件将被重命名和移动。'
setName('Change backlink text when renaming a note'
setName('重命名注释时更改反向链接文本'
setDesc('When the note is renamed, the links to it are updated. If this option is enabled, the text of links to this note will also be changed.'
setDesc('重命名注释后，将更新指向注释的链接。如果启用此选项，指向此备注的链接文本也将更改。'
setName("Ignore folders"
setName("忽略文件夹"
setDesc("List of folders to ignore. Each folder on a new line."
setDesc("要忽略的文件夹列表。每一个文件夹都在一行。"
setName("Ignore files"
setName("忽略文件"
setDesc("List of files to ignore. Each file on a new line."
setDesc("要忽略的文件列表。每个文件都在一行。"
setName("Attachments subfolder"
setName("附件子文件夹"
setDesc("Collect attachments in this subfolder of the note folder (when using the \"
setDesc("在笔记文件夹的此子文件夹中收集附件（使用\ "
setName("Consistant report file name"
setName("一致性报告文件名"
setName("EXPERIMENTAL: Use built-in Obsidian link caching to process moved notes"
setName("实验：使用内置的Obsidian链接缓存处理移动的笔记"
setDesc("Turn it off if plugin misbehaves"
setDesc("如果插件行为不当，请将其关闭"