汉化:Fendi  QQ:381646562
===========================================
name: 'Open emoji picker',
name: '打开表情符号选择器',
containerEl.createEl('h1', { text: 'Emoji Toolbar' });
containerEl.createEl('h1', { text: '表情符号工具栏' });
containerEl.createEl('a', { text: 'Created by oliveryh', href: 'https://github.com/oliveryh/' });
containerEl.createEl('a', { text: 'Oliveryh创建', href: 'https://github.com/oliveryh/' });
containerEl.createEl('h2', { text: 'Settings' });
containerEl.createEl('h2', { text: '设置' });
.setDesc('Improved emoji support. Note: this applies to emoji search and preview only.')
.setDesc('改进了表情符号支持。注意：这仅适用于表情符号搜索和预览。')
